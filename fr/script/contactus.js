$j('form#contact-form').submit(function(){
	$j('form#contact-form .contact-error').remove();
	var hasError = false;
	$j('form#contact-form .requiredField').each(function() {
		if(jQuery.trim($j(this).val()) == '' || jQuery.trim($j(this).val()) == jQuery.trim($j(this).attr('placeholder'))){
			var labelText = $j(this).prev('label').text();
			$j(this).parent().append("<strong class='contact-error'>Ce champ est requis</strong>");
			$j(this).addClass('inputError');
			hasError = true;
		} else { //else 1 
			if($j(this).hasClass('email')) { //if hasClass('email')
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim($j(this).val()))){
					var labelText = $j(this).prev('label').text();
					$j(this).parent().append("<strong class='contact-error'>Veuillez entrer un courriel valide.</strong>");
					$j(this).addClass('inputError');
					hasError = true;
				} 
			} //end of if hasClass('email')

		} // end of else 1 
	}); //end of each()
	
	if(!hasError){
		$j.ajax({
			url: "http://formspree.io/services@appolo-music.com", 
			type: 'POST',
			data: $j(this).serialize(),
			dataType: 'json'
		}).done(function(msg){
			$j("form#contact-form").before("<div class='contact-success'><strong>MERCI!</strong><p>Un courriel a été envoyé avec succès. Nous vous contacterons dès que possible.</p></div>");
			$j("form#contact-form").hide();
		}).fail(function(jqXHR, status)	{
			$j("form#contact-form").before("<div class='contact-success'><strong>Une erreur de serveur est survenue. Veuillez réessayer plus tard.</strong></p></div>");
		});			
	}
	return false;
});