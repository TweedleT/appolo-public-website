document.write("" +
"<footer class=\"footer_border_columns\">"+
"          <div class=\"footer_inner clearfix\">"+
"            <div class=\"footer_top_holder\">"+
"              <div class=\"footer_top\">"+
"                <div class=\"container\">"+
"                  <div class=\"container_inner\">"+
"                    <div class=\"four_columns clearfix\">"+
"                      <div class=\"qode_column column1\">"+
"                        <div class=\"column_inner\">"+
"                          <div id=\"text-2\" class=\"widget widget_text\">"+
"                            <div class=\"textwidget\">"+
"                              <img src=\"img/logo/svg/logo-lite.svg\" alt=\"logo\" width='280'/><br/>"+
"  <a href=\"https://itunes.apple.com/ca/app/appolo-music/id914708343?mt=8\"><img class=\"footer-app-link\"src=\"img/store/appstore.png\" alt=\"Apple Store\"></a>"+
"  <a href=\"https://play.google.com/store/apps/details?id=project.appolo.music\"><img class=\"footer-app-link\"src=\"img/store/googleplay.png\" alt=\"Google Play\"></a>                            "+
"</div>"+
"                          </div>"+
"                        </div>"+
"                      </div>"+
"                      <div class=\"qode_column column2\">"+
"                        <div class=\"column_inner\">"+
"                          <div id=\"nav_menu-4\" class=\"widget widget_nav_menu\">"+
"                            <h4>MORE</h4>"+
"                            <div class=\"menu-footer_menu-container\">"+
"                              <ul id=\"menu-footer_menu-1\" class=\"menu\">"+
"                                <li class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-14829\"><a href=\"contactus.html\">Contact Us</a></li>"+
"                                <li class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-14830\"><a href=\"#\" disabled>FAQ</a></li>                                "+
"                              </ul>"+
"                            </div>"+
"                          </div>"+
"                        </div>"+
"                      </div>"+
"<div class=\"qode_column column3\">"+
"                        <div class=\"column_inner\">"+
"                          <div id=\"nav_menu-4\" class=\"widget widget_nav_menu\">"+
"                            <h4>OTHER CONTENT</h4>"+
"                            <div class=\"menu-footer_menu-container\">"+
"                              <ul id=\"menu-footer_menu-1\" class=\"menu\">"+
"                                <li class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-14830\"><a href=\"index.html\">Fans</a></li> " +
"								 <li class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-14830\"><a href=\"artists.html\">Artists</a></li> " +
"								 <li class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-14830\"><a href=\"venues.html\">Venues</a></li> " +
"								 <li class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-14830\"><a href=\"promoters.html\" disabled>Promoters</a></li> " +
"                              </ul>"+
"                            </div>"+
"                          </div>"+
"                        </div>"+
"                      </div>   "+
"                      <div class=\"qode_column column4\">"+
"                        <div class=\"column_inner\">"+
"                          <div id=\"text-4\" class=\"widget widget_text\">"+
"                            <h4>FOLLOW US</h4>"+
"                            <div class=\"textwidget\">"+
"<span class='q_social_icon_holder with_link circle_social' ><a href='https://www.facebook.com/mucitymusic' target='_blank'><span class='fa-stack tiny circle_social' style='color: #393939;'><span class='social_icon social_facebook'></span></span></a></span>"+
"<span class='q_social_icon_holder with_link circle_social' ><a href='https://twitter.com/mucity_official' target='_blank'><span class='fa-stack tiny circle_social' style='color: #393939;border: 1px solid #;'><span class='social_icon social_twitter'></span></span></a></span>"+
"<span class='q_social_icon_holder with_link circle_social' ><a href='https://www.linkedin.com/company/appolo-music?trk=ppro_cprof' target='_self'><span class='fa-stack tiny circle_social' style='color: #393939;background-color: #;'><span class='social_icon social_linkedin'></span></span></a></span>                              "+
"                            <span class='q_social_icon_holder with_link circle_social' ><a href='https://plus.google.com/106982928955777290475/about' target='_self'><span class='fa-stack tiny circle_social' style='color: #393939;background-color: #;'><span class='social_icon social_googleplus'></span></span></a></span>"+
"</div>"+
"                          </div>"+
"                        </div>"+
"                      </div>"+
"                    </div>"+
"                  </div>"+
"                </div>"+
"              </div>"+
"            </div>"+
"            <div class=\"footer_bottom_holder\">"+
"              <div class=\"footer_bottom\">"+
"                <div class=\"textwidget\"><span style=\"display: block; line-height:14px;\">&copy; Copyright Mucity 2015</span></div>"+
"              </div>"+
"            </div>"+
"          </div>"+
"        </footer>"
);
