$j(document).ready(function() {
	var owl = $j('#owl-example');
	
	$j('#owl-example').owlCarousel({	
		items: 4,
		itemsDesktop: [1200, 3],
		itemsDesktopSmall: [1000, 2],
		itemsMobile: [600, 1],
		autoPlay: 4000,
		stopOnHover: true,
		pagination: false,
		rewindspeed: 4000
	});
	
	$j(".next").click(function(){
		owl.trigger('owl.next');
	})
	
	$j(".prev").click(function(){
		owl.trigger('owl.prev');
	})
});
