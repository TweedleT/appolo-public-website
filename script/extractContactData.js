var parameters = window.location.href.substring(1).split("?")[1].split("#")[0].split("&");

parameters.forEach(function(parameter){
	parameter = parameter.replace(/%/g, " ");
	parameter = parameter.split("=");
	var id = parameter[0];
	var value = parameter[1];
	document.getElementById(id).value = value;
});