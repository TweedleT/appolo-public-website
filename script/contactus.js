$j('form#contact-form').submit(function(){
	$j('form#contact-form .contact-error').remove();
	var hasError = false;
	$j('form#contact-form .requiredField').each(function() {
		if(jQuery.trim($j(this).val()) == '' || jQuery.trim($j(this).val()) == jQuery.trim($j(this).attr('placeholder'))){
			var labelText = $j(this).prev('label').text();
			$j(this).parent().append("<strong class='contact-error'>This is a required field</strong>");
			$j(this).addClass('inputError');
			hasError = true;
		} else { //else 1 
			if($j(this).hasClass('email')) { //if hasClass('email')
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim($j(this).val()))){
					var labelText = $j(this).prev('label').text();
					$j(this).parent().append("<strong class='contact-error'>Please enter a valid email address.</strong>");
					$j(this).addClass('inputError');
					hasError = true;
				} 
			} //end of if hasClass('email')

		} // end of else 1 
	}); //end of each()
	
	if(!hasError){
		$j.ajax({
			url: "http://formspree.io/services@appolo-music.com", 
			type: 'POST',
			data: $j(this).serialize(),
			dataType: 'json'
		}).done(function(msg){
			$j("form#contact-form").before("<div class='contact-success'><strong>THANK YOU!</strong><p>Your email was successfully sent. We will contact you as soon as possible.</p></div>");
			$j("form#contact-form").hide();
		}).fail(function(jqXHR, status)	{
			$j("form#contact-form").before("<div class='contact-success'><strong>Email server problem</strong></p></div>");
		});			
	}
	return false;
});