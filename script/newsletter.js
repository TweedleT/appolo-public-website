$j('#newsletter-form').submit(function() {
	$j.ajax({
		url: "http://formspree.io/services@appolo-music.com", 
		type: 'POST',
		data: $j(this).serialize(),
		dataType: 'json'
	}).done(function(msg){
		$j('#newsletter-form').hide();
		$j('#newsletter-success').show();
	}).fail(function(jqXHR, status)	{
		$j('#newsletter-error').show();
	});
	
	return false;
});