function redirectToFrench() {
	var pageName = window.location.pathname.split("/").pop();
	var pageFr = window.location.pathname.replace("/" + pageName, "/fr/" + pageName);
	window.location.assign(pageFr);
};

function redirectToEnglish() {
	var pageName = window.location.pathname.split("/").pop();
	var pageEn = window.location.pathname.replace("/fr/" + pageName, "/" + pageName);
	window.location.assign(pageEn);
};